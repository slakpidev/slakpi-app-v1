PGDMP         6                w        	   slakpiApp    11.4    11.4                0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    16770 	   slakpiApp    DATABASE     �   CREATE DATABASE "slakpiApp" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Chile.1252' LC_CTYPE = 'Spanish_Chile.1252';
    DROP DATABASE "slakpiApp";
             adminDB    false                       0    0    DATABASE "slakpiApp"    COMMENT     V   COMMENT ON DATABASE "slakpiApp" IS 'Database of the architecture for the Slakpi App';
                  adminDB    false    2845                        2615    16799    administration    SCHEMA        CREATE SCHEMA administration;
    DROP SCHEMA administration;
             postgres    false                       0    0    SCHEMA administration    COMMENT     z   COMMENT ON SCHEMA administration IS 'Content the tables for the all configuration and administration of the application';
                  postgres    false    6            �            1259    16821 	   companies    TABLE     >  CREATE TABLE administration.companies (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    description text,
    status boolean DEFAULT true,
    type_comp character varying(150),
    created_at timestamp without time zone DEFAULT now(),
    lastupdate timestamp without time zone DEFAULT now()
);
 %   DROP TABLE administration.companies;
       administration         adminDB    false    6            �            1259    16819    company_id_seq    SEQUENCE     �   ALTER TABLE administration.companies ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME administration.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            administration       adminDB    false    6    204            �            1259    16812    tickets    TABLE     �   CREATE TABLE administration.tickets (
    id integer NOT NULL,
    ref_number character varying(255),
    title character varying(155)
);
 #   DROP TABLE administration.tickets;
       administration         adminDB    false    6            �            1259    16817    tickets_id_seq    SEQUENCE     �   ALTER TABLE administration.tickets ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME administration.tickets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            administration       adminDB    false    201    6            �            1259    16800    users    TABLE     I  CREATE TABLE administration.users (
    id integer NOT NULL,
    first_name character varying(120),
    last_name character varying(120),
    email character varying(255),
    password character varying(120),
    "createdAt" timestamp without time zone DEFAULT now(),
    "updatedAt" timestamp without time zone DEFAULT now()
);
 !   DROP TABLE administration.users;
       administration         adminDB    false    6            �            1259    16808    users_id_seq    SEQUENCE     �   ALTER TABLE administration.users ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME administration.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            administration       adminDB    false    199    6                      0    16821 	   companies 
   TABLE DATA               m   COPY administration.companies (id, name, description, status, type_comp, created_at, lastupdate) FROM stdin;
    administration       adminDB    false    204   /                 0    16812    tickets 
   TABLE DATA               @   COPY administration.tickets (id, ref_number, title) FROM stdin;
    administration       adminDB    false    201   �                 0    16800    users 
   TABLE DATA               m   COPY administration.users (id, first_name, last_name, email, password, "createdAt", "updatedAt") FROM stdin;
    administration       adminDB    false    199   	                   0    0    company_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('administration.company_id_seq', 1, true);
            administration       adminDB    false    203            !           0    0    tickets_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('administration.tickets_id_seq', 3, true);
            administration       adminDB    false    202            "           0    0    users_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('administration.users_id_seq', 8, true);
            administration       adminDB    false    200            �
           2606    16831    companies company_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY administration.companies
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY administration.companies DROP CONSTRAINT company_pkey;
       administration         adminDB    false    204            �
           2606    16816    tickets tickets_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY administration.tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY administration.tickets DROP CONSTRAINT tickets_pkey;
       administration         adminDB    false    201            �
           2606    16811    users users_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY administration.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 B   ALTER TABLE ONLY administration.users DROP CONSTRAINT users_pkey;
       administration         adminDB    false    199               ]   x�3�����/����tI-NIUH�Q(�,.I�MT�I�L/�/�K�,�(�/KMM�/�420��5��54V02�20�21Գ42�4��#����� I>         ]   x�3�4426461261�I-.Q�OSp/J,���2I���q����*��(g���&*�$V���psB TUPjbr��cAW� 
��         Q  x����N�@Fם�p��q�K;��@[.�DI4n*��%���|
_L�,�h���LN�X�}�V���Ϗ�ʞ�,�ڮ��M���ڢ������izA�DS����b�;[g�9�mU�������?�`�
e���Y����hlTS�)u�	���f4��Ѡ�[�*�!i�y]Í���0����KNDB��u�0�Sӂ�%-�����7.��A��pqy�&��-/���̼I%�b߃��.�qo�������mΡ�n@�1U�'M��#.�e��������|���F>v��Q��8T���<�a�����G=��H�`�W�7A�!�*�֑     