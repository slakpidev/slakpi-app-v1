PGDMP          ;             	    w        	   slakpiApp    12.0    12.0 *    {           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            |           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            }           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            ~           1262    16427 	   slakpiApp    DATABASE     i   CREATE DATABASE "slakpiApp" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'C' LC_CTYPE = 'C';
    DROP DATABASE "slakpiApp";
                adminDB    false                       0    0    DATABASE "slakpiApp"    COMMENT     :   COMMENT ON DATABASE "slakpiApp" IS 'Database Slakpi App';
                   adminDB    false    3198                        2615    16428    administration    SCHEMA        CREATE SCHEMA administration;
    DROP SCHEMA administration;
                postgres    false            �           0    0    SCHEMA administration    COMMENT     z   COMMENT ON SCHEMA administration IS 'Content the tables for the all configuration and administration of the application';
                   postgres    false    7            �            1259    16429 	   companies    TABLE     >  CREATE TABLE administration.companies (
    id integer NOT NULL,
    name character varying(150) NOT NULL,
    description text,
    status boolean DEFAULT true,
    type_comp character varying(150),
    created_at timestamp without time zone DEFAULT now(),
    lastupdate timestamp without time zone DEFAULT now()
);
 %   DROP TABLE administration.companies;
       administration         heap    adminDB    false    7            �            1259    16438    company_id_seq    SEQUENCE     �   ALTER TABLE administration.companies ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME administration.company_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            administration          adminDB    false    7    203            �            1259    16650    contacts    TABLE     ?  CREATE TABLE administration.contacts (
    id integer NOT NULL,
    first_name character varying,
    last_name character varying,
    cell_phone character varying,
    email character varying,
    number_phone character varying,
    created_at timestamp without time zone,
    update_at timestamp without time zone
);
 $   DROP TABLE administration.contacts;
       administration         heap    postgres    false    7            �            1259    16648    contacts_id_seq    SEQUENCE     �   CREATE SEQUENCE administration.contacts_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE administration.contacts_id_seq;
       administration          postgres    false    7    210            �           0    0    contacts_id_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE administration.contacts_id_seq OWNED BY administration.contacts.id;
          administration          postgres    false    209            �            1259    16440    tickets    TABLE     �   CREATE TABLE administration.tickets (
    id integer NOT NULL,
    ref_number character varying(255),
    title character varying(155)
);
 #   DROP TABLE administration.tickets;
       administration         heap    adminDB    false    7            �            1259    16443    tickets_id_seq    SEQUENCE     �   ALTER TABLE administration.tickets ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME administration.tickets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            administration          adminDB    false    7    205            �            1259    16663    users    TABLE     '  CREATE TABLE administration.users (
    id integer NOT NULL,
    username character varying NOT NULL,
    password character varying,
    reset_password character varying,
    contact_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);
 !   DROP TABLE administration.users;
       administration         heap    postgres    false    7            �            1259    16445 	   users_bkp    TABLE     M  CREATE TABLE administration.users_bkp (
    id integer NOT NULL,
    first_name character varying(120),
    last_name character varying(120),
    email character varying(255),
    password character varying(120),
    "createdAt" timestamp without time zone DEFAULT now(),
    "updatedAt" timestamp without time zone DEFAULT now()
);
 %   DROP TABLE administration.users_bkp;
       administration         heap    adminDB    false    7            �            1259    16661    users_contact_id_seq    SEQUENCE     �   CREATE SEQUENCE administration.users_contact_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE administration.users_contact_id_seq;
       administration          postgres    false    213    7            �           0    0    users_contact_id_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE administration.users_contact_id_seq OWNED BY administration.users.contact_id;
          administration          postgres    false    212            �            1259    16453    users_id_seq    SEQUENCE     �   ALTER TABLE administration.users_bkp ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME administration.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            administration          adminDB    false    7    207            �            1259    16659    users_id_seq1    SEQUENCE     �   CREATE SEQUENCE administration.users_id_seq1
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE administration.users_id_seq1;
       administration          postgres    false    213    7            �           0    0    users_id_seq1    SEQUENCE OWNED BY     N   ALTER SEQUENCE administration.users_id_seq1 OWNED BY administration.users.id;
          administration          postgres    false    211            �           2604    16653    contacts id    DEFAULT     z   ALTER TABLE ONLY administration.contacts ALTER COLUMN id SET DEFAULT nextval('administration.contacts_id_seq'::regclass);
 B   ALTER TABLE administration.contacts ALTER COLUMN id DROP DEFAULT;
       administration          postgres    false    209    210    210            �           2604    16666    users id    DEFAULT     u   ALTER TABLE ONLY administration.users ALTER COLUMN id SET DEFAULT nextval('administration.users_id_seq1'::regclass);
 ?   ALTER TABLE administration.users ALTER COLUMN id DROP DEFAULT;
       administration          postgres    false    213    211    213            �           2604    16667    users contact_id    DEFAULT     �   ALTER TABLE ONLY administration.users ALTER COLUMN contact_id SET DEFAULT nextval('administration.users_contact_id_seq'::regclass);
 G   ALTER TABLE administration.users ALTER COLUMN contact_id DROP DEFAULT;
       administration          postgres    false    212    213    213            n          0    16429 	   companies 
   TABLE DATA           m   COPY administration.companies (id, name, description, status, type_comp, created_at, lastupdate) FROM stdin;
    administration          adminDB    false    203   �1       u          0    16650    contacts 
   TABLE DATA           }   COPY administration.contacts (id, first_name, last_name, cell_phone, email, number_phone, created_at, update_at) FROM stdin;
    administration          postgres    false    210   �1       p          0    16440    tickets 
   TABLE DATA           @   COPY administration.tickets (id, ref_number, title) FROM stdin;
    administration          adminDB    false    205   c2       x          0    16663    users 
   TABLE DATA           s   COPY administration.users (id, username, password, reset_password, contact_id, created_at, updated_at) FROM stdin;
    administration          postgres    false    213   �2       r          0    16445 	   users_bkp 
   TABLE DATA           q   COPY administration.users_bkp (id, first_name, last_name, email, password, "createdAt", "updatedAt") FROM stdin;
    administration          adminDB    false    207   Q3       �           0    0    company_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('administration.company_id_seq', 1, true);
          administration          adminDB    false    204            �           0    0    contacts_id_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('administration.contacts_id_seq', 1, true);
          administration          postgres    false    209            �           0    0    tickets_id_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('administration.tickets_id_seq', 3, true);
          administration          adminDB    false    206            �           0    0    users_contact_id_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('administration.users_contact_id_seq', 1, false);
          administration          postgres    false    212            �           0    0    users_id_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('administration.users_id_seq', 8, true);
          administration          adminDB    false    208            �           0    0    users_id_seq1    SEQUENCE SET     C   SELECT pg_catalog.setval('administration.users_id_seq1', 1, true);
          administration          postgres    false    211            �           2606    16456    companies company_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY administration.companies
    ADD CONSTRAINT company_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY administration.companies DROP CONSTRAINT company_pkey;
       administration            adminDB    false    203            �           2606    16658    contacts contact_pk 
   CONSTRAINT     Y   ALTER TABLE ONLY administration.contacts
    ADD CONSTRAINT contact_pk PRIMARY KEY (id);
 E   ALTER TABLE ONLY administration.contacts DROP CONSTRAINT contact_pk;
       administration            postgres    false    210            �           2606    16458    tickets tickets_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY administration.tickets
    ADD CONSTRAINT tickets_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY administration.tickets DROP CONSTRAINT tickets_pkey;
       administration            adminDB    false    205            �           2606    16672    users users_pk 
   CONSTRAINT     T   ALTER TABLE ONLY administration.users
    ADD CONSTRAINT users_pk PRIMARY KEY (id);
 @   ALTER TABLE ONLY administration.users DROP CONSTRAINT users_pk;
       administration            postgres    false    213            �           2606    16460    users_bkp users_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY administration.users_bkp
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 F   ALTER TABLE ONLY administration.users_bkp DROP CONSTRAINT users_pkey;
       administration            adminDB    false    207            �           2606    16674    users users_username_key 
   CONSTRAINT     _   ALTER TABLE ONLY administration.users
    ADD CONSTRAINT users_username_key UNIQUE (username);
 J   ALTER TABLE ONLY administration.users DROP CONSTRAINT users_username_key;
       administration            postgres    false    213            �           2606    16675    users users_fk0    FK CONSTRAINT     �   ALTER TABLE ONLY administration.users
    ADD CONSTRAINT users_fk0 FOREIGN KEY (contact_id) REFERENCES administration.contacts(id);
 A   ALTER TABLE ONLY administration.users DROP CONSTRAINT users_fk0;
       administration          postgres    false    213    210    3050            n   ]   x�3�����/����tI-NIUH�Q(�,.I�MT�I�L/�/�K�,�(�/KMM�/�420��5��54V02�20�21Գ42�4��#����� I>      u   f   x�3�tL����,.)JL�/��Rs9�M�,M�L,M�9AJ�s���K��I�	pZ���*ZZ�[�虚�[������� 	2�      p   ]   x�3�4426461261�I-.Q�OSp/J,���2I���q����*��(g���&*�$V���psB TUPjbr��cAW� 
��      x   q   x�3�LL����T1JR14P�0�r
s�ϫ��q4��Џt�/r���(/�ssJq+1��ss5�-����s	�)q��4�420��54�54U0��21�2��3�4�0�'����� =� -      r   Q  x����N�@Fם�p��q�K;��@[.�DI4n*��%���|
_L�,�h���LN�X�}�V���Ϗ�ʞ�,�ڮ��M���ڢ������izA�DS����b�;[g�9�mU�������?�`�
e���Y����hlTS�)u�	���f4��Ѡ�[�*�!i�y]Í���0����KNDB��u�0�Sӂ�%-�����7.��A��pqy�&��-/���̼I%�b߃��.�qo�������mΡ�n@�1U�'M��#.�e��������|���F>v��Q��8T���<�a�����G=��H�`�W�7A�!�*�֑     