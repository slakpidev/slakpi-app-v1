import { gql } from 'apollo-server-express';

const companies = gql`
type Company {
  id: ID
  name: String!
  description: String
  status: String
  type_comp: String
  ubication: String
  created_at:  String
  lastupdate: String
}

input CompanyInput {
  id: ID
  name: String!
  description: String
  status: String
  ubication: String
  type_comp: String
}

""" Query """

type Query {
  getAllCompanies: [Company]
}

 """ Mutations """
type Mutation {

  """ Create Company """
    createCompany(input: CompanyInput) : Company
}
`;

module.exports = companies;