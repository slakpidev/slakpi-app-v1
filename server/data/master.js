import { gql } from 'apollo-server-express';

const master = gql`
    type User {
  id: ID
  first_name: String!
  last_name: String!
  email: String!
  password: String!
}

type UserLogin {
  first_name: String
  last_name: String
  email: String
}

type Dias {
  name: String
}

type Date {
  id: ID
  date: String!
  diasSemana: [Dias]
}


input UserInput {
  id: ID
  first_name: String!
  last_name: String!
  email: String!
  password: String!
}

type Ticket {
  id: ID
  ref_number: String!
  title: String!
}

input TicketInput {
  id: ID
  ref_number: String!
  title: String!
}


input LoginInput {
  email: String!
  password: String!
}

type Token {
  token: String!
}

""" Query """

type Query {
  getAllUsers: [User]
  getUserLogin: UserLogin  
}

 """ Mutations """
type Mutation {
  """ Create User """
    createUser(input: UserInput) : User

  """ Update User """
    updateUser(input: UserInput) : User

  """ Login User """
    loginUser(input: LoginInput) : Token

  """ Create Ticket """
    createTicket(input: TicketInput) : Ticket
}
`;

module.exports = master;