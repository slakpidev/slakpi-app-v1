
import Sequelize from 'sequelize';
import { rejects } from 'assert';
import bcrypt from 'bcrypt';

import env from 'node-env-file';
env(__dirname + '/../config/.env');

// Generate Token
import jwt from 'jsonwebtoken';

// Models
import User from '../models/User';
import Ticket from '../models/Ticket';
import Company from '../models/Company';

const tokenCreate = (userLogin, secret, expiresIn) => {
    const {username} = userLogin;
    return jwt.sign({username}, secret, {expiresIn});
}

export const resolvers = {
    Query : {        
        getAllUsers: (root) => {
            const Users= User.findAll({
                // offset: 5, limit: 5, 
                order: [
                    ['id', 'DESC']
                ]
            })
            console.log(Users)
            return Users;
        },
        getUserLogin: async (root, args, {userLogin}) => {
            
            if(!userLogin) {
                return null;
            }
            const user = await User.findOne({
                where: {username: userLogin.username},
            })

            return user;
        },
        getAllCompanies: (root) => {
            return Company.findAll({
                order: [
                    ['id', 'DESC']
                ]
            })
        }
    },
    Mutation: {
      
        loginUser: async (root, {input}) => {
            const userLogin = await User.findOne({
                where: {username: input.username},
                attributes: ['id', 'username','password']
            })
            if(!userLogin){
                throw new Error('Usuario no registrado.')
            }else{
               const login = await bcrypt.compare(input.password, userLogin.password);
               if(login){
                return {
                    token: tokenCreate(userLogin, process.env.TOKEN_SECRET, '1hr')
                }
               }else{
                   throw new Error('Usuario o Password Incorrectos')
               }
            }
        },
        createUser: async (root, {input}) => {
            const userInDB = await User.findOne({
                where: {email: input.email},
                attributes: ['id']
            })
            if(userInDB){
                throw new Error('User already exists')
            }else{
              const passwordHash = await bcrypt.hash(input.password, process.env.BCRYPT_SALT_ROUNDS);
              return User.create({                
                first_name : input.first_name,
                last_name : input.last_name,
                email : input.email,
                password : passwordHash,
                }).catch(err => {
                    console.error('Error to create User:', err);
                });
            }
        },

        createTicket: async (root, {input}) => {
            if(input.ref_number === '' || input.title === ''){
                throw new Error("The input can't empty...") 
            }else{
                return Ticket.create({                
                    ref_number : input.ref_number,
                    title : input.title
                }).catch(err => {
                    console.error('Error to create the Ticket:', err);
                });
            }
        },

        createCompany: async (root, {input}) => {
            if(input.name === '' || input.type_comp === ''){
                throw new Error("The input can't empty...") 
            }else{
                return Company.create({                
                    name : input.name,
                    description :input.description,
                    status :input.status,
                    type_comp :input.type_comp,
                    ubication :input.ubication
                }).catch(err => {
                    console.error('Error to create the Company:', err);
                });
            }
        },

        updateUser: async (root, {input}) => {
            const userDataInput = await new User({
                id: input.id,
                first_name : input.first_name,
                last_name : input.last_name,
                email : input.email,
                password: input.password
              });
            //   Por alguna razón se debe colocar dos veces el return
           return User.update(userDataInput, {returning: true,
                where: {
                    id: input.id
                },
            }).then(user => {
                return userDataInput;
            })
            .catch(err => {
                console.error('Error to update User:', err);
            });            
        }
    }
}
 
export default resolvers;