import express from 'express';
import exphbs from 'express-handlebars';
import env from 'node-env-file';
import db from './config/db';
import { ApolloServer, gql } from 'apollo-server-express';
env(__dirname + '/config/.env');

// GraphQL
import { typeDefs } from './data/schema';
import { resolvers } from './data/resolvers';


// Generate Token
import jwt from 'jsonwebtoken';

const app = express(); 

var port = process.env.PORT || '5000';

const server = new ApolloServer({ 
  typeDefs, 
  resolvers,
  context: async ({req}) => {
    // Get Token From server
    const token = req.headers['authorization'];

    if(token !== 'null'){
      try {

        // We verify the token from the Frontend (client)
        const userLogin = await jwt.verify(token, process.env.TOKEN_SECRET);

        // Add the userLogin to the request
        req.userLogin= userLogin;

        // Toca traer los datos del Usuario
        // Para ello debo crear un resolver de usuarios

        // console.log(token)
        // console.log(userLogin)
        return {
          userLogin
        }        
      } catch (error) {
        console.error(error)
      }
    }
  } });

server.applyMiddleware({app});

   db
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

app.listen(port, console.log(`Server started on port http://localhost:${port}${server.graphqlPath}`));