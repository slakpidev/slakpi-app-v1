import Sequelize from 'sequelize';
import db from '../config/db';

const Ticket = db.define('tickets', {
     // attributes
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    // attributes
    ref_number: {
        type: Sequelize.STRING
    },
    title: {
        type: Sequelize.STRING
    },
},
{
  schema: 'administration',
  timestamps: false
  }
);

export default Ticket;