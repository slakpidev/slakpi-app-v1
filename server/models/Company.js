import Sequelize from 'sequelize';
import db from '../config/db';

const Company = db.define('companies', {
     // attributes
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    // attributes
    name: {
        type: Sequelize.STRING
    },
    description: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.BOOLEAN
    },
    type_comp: {
        type: Sequelize.STRING
    },
    ubication: {
        type: Sequelize.STRING
    },
    created_at: Sequelize.DATE,
    lastupdate: Sequelize.DATE,

},
{
  schema: 'administration',
  timestamps: false
  }
);

export default Company;