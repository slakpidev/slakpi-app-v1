import Sequelize from 'sequelize';
import db from '../config/db';

const User = db.define('users', {
  // attributes
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  // attributes
  username: {
    type: Sequelize.STRING
  },
  password: {
    type: Sequelize.STRING
    // allowNull defaults to true
  },
  contact_id: {
    type: Sequelize.INTEGER,
  },
  reset_password: {
    type: Sequelize.STRING
    // allowNull defaults to true
  },
},
{
  schema: 'administration',
  timestamps: false
  }
);

export default User;