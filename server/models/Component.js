import Sequelize from 'sequelize';
import db from '../config/db';

const Component = db.define('component', {
     // attributes
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    // attributes
    name: {
        type: Sequelize.STRING
    },
    component_type: {
        type: Sequelize.STRING
    },
    code: {
        type: Sequelize.STRING
    },
    status: {
        type: Sequelize.BOOLEAN
    },
    company: {
        type: Sequelize.STRING
    },
    departament: {
        type: Sequelize.STRING
    },
    location: {
        type: Sequelize.STRING
    },
    created_at: Sequelize.DATE,
    lastupdate: Sequelize.DATE,

},
{
  schema: 'administration',
  timestamps: false
  }
);

export default Component;