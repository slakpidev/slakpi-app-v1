import Sequelize from 'sequelize';
import db from '../config/db';

const Contact = db.define('contacts', {
  // attributes
  id: {
    type: Sequelize.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  // attributes
  first_name: {
    type: Sequelize.STRING
  },
  last_name: {
    type: Sequelize.STRING
    // allowNull defaults to true
  },
  cell_phone: {
    type: Sequelize.STRING
    // allowNull defaults to true
  },
  email: {
    type: Sequelize.STRING
    // allowNull defaults to true
  },
  number_phone: {
    type: Sequelize.STRING
    // allowNull defaults to true
  },
},
{
  schema: 'administration',
  timestamps: false
  }
);

Contact.associate = (models) => {
  Contact.belongsTo(models.User,{
    through: 'users',
    foreingKey: 'contact_id' 
  })
};

export default Contact;