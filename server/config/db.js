import Sequelize from 'sequelize';
import env from 'node-env-file';
env(__dirname + '/.env');

const database = process.env.DB_DATABASE;
const host = process.env.DB_HOST;
const userDb = process.env.DB_USER;
const userPass = process.env.DB_PASS;
const dialectDb = process.env.DB_DIALECT;


module.exports = new Sequelize(database, userDb, userPass, {
    host: host,
    dialect: dialectDb,
    pool: {
        max: 5,
        min: 0,
        acquire: 30000,
        idle: 10000
      }
  });