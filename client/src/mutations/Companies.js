import gql from 'graphql-tag';

export const NEW_COMPANIES = gql `
   mutation createCompany($input: CompanyInput){
        createCompany(input:$input){
            id
            name
            description
            status
            type_comp
        }
    }
`;