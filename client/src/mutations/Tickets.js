import gql from 'graphql-tag';

export const NEW_TICKET = gql `
    mutation createTicket($input: TicketInput){
        createTicket(input:$input){
            id
            ref_number
            title
        }
    }
`;