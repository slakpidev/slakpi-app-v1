import gql from 'graphql-tag';

export const ACCESS_LOGIN = gql `
    mutation loginUser($input: LoginInput){
    loginUser(input:$input)
        {
            token
        }
    }
`;