import React, {Fragment, useState} from 'react';
import { BrowserRouter as Router, Redirect  } from "react-router-dom";
import Sidebar from './components/elements/Sidebar';
import Header from './components/elements/Header';
import Footer from './components/elements/Footer';
import Session from './components/elements/Session';
import Routes from './components/elements/Routes';
import ArrayRoute from './components/elements/ArrayRoute';
import { Animated } from 'react-animated-css';

const App = ({refetch, session }) => {

  console.log(session);
  

  const { getUserLogin } = session;

  const userLogin = (getUserLogin) ? true : <Redirect to="/login" />;
  
    // State for the class Toggle

  const [toggleClass, addClassToggle] = useState('');

  const sideBarMenu = toggle => {
    addClassToggle({
      toggleClass : toggle
    })
  }

  return (
      <Router>
        <Fragment>
          <Animated animationIn="fadeIn" animationInDuration={1000} animationOut="fadeOutDown" animationOutDuration={1000}>
            {/* validation for the User Login, if isset session the user is true, else redirect to the /login */}
            {userLogin}
            {/* Page Wrapper */}
            <div id="wrapper">
            <Sidebar toggleClass={toggleClass} session={session} ArrayRoute = {ArrayRoute}/>
              {/* Content Wrapper */}
              <div id="content-wrapper" className="d-flex flex-column bg-black">
                {/* Main Content */}
                <div id="content">
                  {/* Header */}                
                  <Header sideBarMenu = {sideBarMenu} session={session} />
                    {/* Begin Page Content */}
                    <div className="container-fluid pb-3">
                        <Routes refetch = { refetch } session = { session }  ArrayRoute = {ArrayRoute} />
                      {/* Content main */}
                    </div>
                    {/* /.container-fluid */}
                </div>
              {/* End of Main Content */}
              <Footer session = { session } />
              </div>
              {/* End of Content Wrapper */}
            </div>
            {/* End of Page Wrapper */}
            </Animated>
        </Fragment>
      </Router>
  );
}; 
const RootSession = Session(App);

export { RootSession }