import gql from 'graphql-tag';

export const USER_LOGIN = gql `
    query getUserLogin {
        getUserLogin{
            username
          contactLogin{
            first_name
            last_name
          }
        }
    } 
`;