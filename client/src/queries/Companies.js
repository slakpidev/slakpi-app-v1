import gql from 'graphql-tag';


export const GET_COMPANIES = gql `
    query getAllCompanies{
        getAllCompanies{
            id
            name
            status
            description
            ubication
            type_comp
            created_at
        }
    }
`;