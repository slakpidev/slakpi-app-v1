import React, {Fragment} from 'react';

const Chatnotifications = () => {
    return (
        <Fragment>
            <div className="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in bg-gray-500" aria-labelledby="chatDropdown">
                <h6 className="dropdown-header bg-warning border-0">
                    CHAT
                </h6>
                <a className="dropdown-item d-flex align-items-center" href="#!">
                <div className="dropdown-list-image mr-3">
                    <img className="rounded-circle" src="https://source.unsplash.com/fn_BT9fwg_E/60x60" alt="fake" />
                    <div className="status-indicator bg-success" />
                </div>
                <div className="font-weight-bold">
                    <div className="text-truncate">Hola. Estas?</div>
                    <div className="small text-gray-600">Reinaldo Contreras · 1m</div>
                </div>
                </a>
                <a className="dropdown-item d-flex align-items-center" href="#!">
                <div className="dropdown-list-image mr-3">
                    <img className="rounded-circle" src="https://source.unsplash.com/AU4VPcFN4LE/60x60" alt="fake" />
                    <div className="status-indicator" />
                </div>
                <div>
                    <div className="text-truncate">Tienes disponibilidad para las 2?</div>
                    <div className="small text-gray-600">Carlos Soto · 7m</div>
                </div>
                </a>
                <a className="dropdown-item d-flex align-items-center" href="#!">
                <div className="dropdown-list-image mr-3">
                    <img className="rounded-circle" src="https://source.unsplash.com/CS2uCrpNzJY/60x60" alt="fake" />
                    <div className="status-indicator bg-warning" />
                </div>
                <div>
                    <div className="text-truncate">Excelente!!</div>
                    <div className="small text-gray-600">Pia Alvarez · 30m</div>
                </div>
                </a>
                <a className="dropdown-item d-flex align-items-center" href="#!">
                <div className="dropdown-list-image mr-3">
                    <img className="rounded-circle" src="https://source.unsplash.com/Mv9hjnEUHR4/60x60" alt="fake" />
                    <div className="status-indicator bg-success" />
                </div>
                <div>
                    <div className="text-truncate">mmmm...no lo tengo claro</div>
                    <div className="small text-gray-600">Camila Silva · 1h</div>
                </div>
                </a>
                <a className="dropdown-item text-center small text-gray-600" href="#!">Abrir Chat</a>
            </div>
        </Fragment>
    );
};

export default Chatnotifications;