import React, { Fragment } from 'react';

const Dashboard = () => {
    return (
        <Fragment>
            <h1 className="h3 mb-0 text-gray-700">Management Dashboard </h1>
            <a href="cmdb-orgs-create.html" className="btn btn-black2 btn-icon-split">
            <span className="icon text-white-50">
                <i className="fas fa-download" /></span>
            <span className="text">Generar reportes</span></a>
        </Fragment>
    );
};

export default Dashboard;