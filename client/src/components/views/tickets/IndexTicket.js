import React, { Fragment } from 'react';

const IndexTicket = () => {
    return (
        <Fragment>
            <div className ="bg-black2 border-0 card shadow mb-4 w-100" >
                <div className="card-body">
                    <a className="btn btn-sm btn-black" href="/tickets/create" role="button" id="btnCrearTicket">
                        <span className="text"><i className="fas fa-plus-square"></i> Crear Ticket</span>
                    </a>
                </div>
            </div>
        </Fragment>
    );
};

export default IndexTicket;