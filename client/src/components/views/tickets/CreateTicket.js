import React, { Fragment, useState } from 'react';
import { NEW_TICKET } from '../../../mutations/Tickets';
import { useMutation } from '@apollo/react-hooks';

const CreateTicket = () => {

    const [ticket, addTicket] = useState({
        ref_number: '',
        title: ''
    })

    // Función que actualizará el State
    const updateStateTicket = e => {
        addTicket({
            ...ticket,
            [e.target.name] : e.target.value
        });
    }
    
    // Se asigna el objeto GQL que contiene el mutation a una constante, este contiene el mutations declarado en el Schema graphQL
    const [createTicket] = useMutation(NEW_TICKET);


    // Función encargada de guardar los tickets con el uso de apollo/react-hooks
    const saveTicket = e =>{
        e.preventDefault();

        // Llamamos el mutation creado y asignamos al input el objeto que contiene los inputs del State
        createTicket({ variables: { input: ticket } });
    }

    return (
        <Fragment>
            {/* Page Heading */}
            <div className="col-md-12">
                <div className="d-sm-flex align-items-center justify-content-between mb-4">
                    <h1 className="h3 mb-0 text-white">Creación: Ticket</h1>                
                </div>
                <div className="bg-black2 border-0 card shadow mb-4 w-100">
                    <div className="card-body">
                        <form onSubmit={saveTicket}>
                            <div className="row">
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label className="text-gray-600" htmlFor="txtAsunto">Número de referencia</label>
                                        <input 
                                            type="text" 
                                            className="bg-black text-gray-400 form-control" 
                                            placeholder="Ingrese el número de Referencia" 
                                            id="ref_number"
                                            name="ref_number"
                                            onChange = {updateStateTicket}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-6">
                                    <div className="form-group">
                                        <label className="text-gray-600" htmlFor="txtAsunto">Título</label>
                                        <input 
                                            type="text" 
                                            className="bg-black text-gray-400 form-control" 
                                            placeholder="Ingrese el título" 
                                            id="title"
                                            name="title"
                                            onChange = {updateStateTicket}
                                        />
                                    </div>
                                </div>
                                <div className="col-md-12">
                                    <button 
                                        type="submit" 
                                        className="btn btn-outline-success"                                        
                                    >
                                        Crear Ticket
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default CreateTicket;