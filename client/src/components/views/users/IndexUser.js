import React, { Fragment } from 'react';

const IndexUser = () => {
    return (
        <Fragment>
            <div className ="bg-black2 border-0 card shadow mb-4 w-100" >
                <div className="card-body">
                    <a className="btn btn-sm btn-black" href="/users/create" role="button" id="btnCrearTicket">
                        <span className="text"><i className="fas fa-user-plus"></i> Crear Usuario</span>
                    </a>
                </div>
            </div>
        </Fragment>
    );
};

export default IndexUser;