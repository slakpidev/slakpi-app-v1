import React, { Fragment, useState} from 'react';
import {ACCESS_LOGIN} from '../../../mutations/Users';
import { useMutation } from '@apollo/react-hooks';
import { withRouter } from 'react-router-dom';
import { Animated } from 'react-animated-css';
import ErrorMessages from '../../elements/ErrorMessages';
import { Spinner } from 'react-bootstrap';

const Login = (props) => {

    // Redirect to home when the session isset
    if(props.session.getUserLogin){
        props.history.push('/');
    }

    const initialState = {
        username: '',
        password: '',

    };

    const typeMessage = 'warning';
    const titleMessage = 'Advertencia';
    
    const [login, addLogin] = useState(initialState)
    const [errorMessage, setErrorMessage] = useState(false);
    const [isVisble, setIsVIsible] = useState(true);
    const [loadingShow, setLoadingShow] = useState(false);

    const LoginText = () => (
       <span>
            Login <i className="fas fa-sign-in-alt"></i>
       </span>
    )

    // Función que actualizará el State
    const updateStateLogin = e => {
        setErrorMessage(false)
        setLoadingShow(false)
        addLogin({
            ...login,
            [e.target.name] : e.target.value
        });
    }

    // Se asigna el objeto GQL que contiene el mutations a una constante, este contiene el mutations declarado en el Schema graphQL
    const [loginUser, { data, error }] = useMutation(
        ACCESS_LOGIN,{
            async onCompleted({ loginUser }) {
                localStorage.setItem('token', loginUser.token);
                props.refetch();

                addLogin({
                    ...login
                });
                setIsVIsible(false)
                setTimeout(() => {
                    props.history.push('/dashboard');
                }, 3000);
            }
        });


    // Función encargada de guardar los tickets con el uso de apollo/react-hooks
    const accessLogin = e =>{
        e.preventDefault();

        if(login.username === '' || login.password === ''){
            setErrorMessage(true)
        }else{
            setLoadingShow(true)
            // Llamamos el mutation creado y asignamos al input el objeto que contiene los inputs del State
            console.log(login)
           loginUser({ variables: { input: login } });
        }
    }

    return (
        <Fragment>

        {/* Outer Row */}
        <Animated animationIn="fadeInDown" animationInDuration={2000} animationOut="fadeOut" animationOutDuration={500} isVisible={isVisble}>

            {/* If isset Error, show the toast with the message and the type of style */}
            { (errorMessage) ? <ErrorMessages type = {typeMessage} title = {titleMessage} message = 'Los campos no pueden estar vacíos.' /> : ''}
            { (error) ? <ErrorMessages type = {typeMessage} title = {titleMessage} message = {error.graphQLErrors[0].message} /> : ''}

            <div className="d-flex justify-content-center w-100 mt-5">
                <div className="row">
                    <div className="col-md-12">
                        <div className="bg-black2 border-0 card shadow mb-4 w-100">
                            <div className="bg-black border-0 d-block card-header py-3 text-center">
                                <img src="/assets/images/site-header-logo.png"alt="Slakpi logo" className="w-25"/>
                            </div>
                            <div className="card-body">
                                <form onSubmit={accessLogin}>
                                    <div className="row">
                                        <div className="col-md-6">
                                            <div className="input-group mb-2">
                                                <div className="input-group-prepend">
                                                    <div className="input-group-text bg-secondary text-gray-400"><i className="fas fa-user-tie"></i></div>
                                                </div>
                                                <input 
                                                    type="text" 
                                                    className= {(errorMessage) ? 'bg-black text-gray-400 form-control is-invalid' : 'bg-black text-gray-400 form-control'}
                                                    placeholder="Ingrese su usuario" 
                                                    id="username"
                                                    name="username"
                                                    onChange = {updateStateLogin}
                                                 />
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="input-group mb-2">
                                                <div className="input-group-prepend">
                                                    <div className="input-group-text bg-secondary text-gray-400"><i className="fas fa-lock"></i></div>
                                                </div>
                                                <input 
                                                    type="password" 
                                                    className={(errorMessage) ? 'bg-black text-gray-400 form-control is-invalid' : 'bg-black text-gray-400 form-control'}
                                                    placeholder="Ingrese su contraseña" 
                                                    id="password"
                                                    name="password"
                                                    onChange = {updateStateLogin}
                                                />
                                            </div>
                                        </div>
                                        <div className="col-md-12 text-center">
                                            <button 
                                                type="submit" 
                                                className="btn btn-outline-success btn-block">
                                                    {(loadingShow) ? <Spinner as="span" animation="grow" size="md" role="status" aria-hidden="true" /> : <LoginText /> }                                            
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Animated>
    </Fragment>
    );
};

export default withRouter(Login);