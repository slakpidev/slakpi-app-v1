import React, { Fragment } from 'react';
import { ApolloConsumer } from 'react-apollo';
import { withRouter} from 'react-router-dom';
import { Button } from 'react-bootstrap';

const closeSession = (client, history) => {
    localStorage.removeItem('token', '');
    client.resetStore();

    history.push('/login');
}

const Logout = ({history}) => (
    <ApolloConsumer>
        {client => {
            return (
                <Fragment>
                    <Button 
                    onClick={ () => closeSession(client, history)}
                    className="btn btn-black2">
                        Logout
                    </Button>
                </Fragment>
            );
        }}
    </ApolloConsumer>
);

export default withRouter(Logout);