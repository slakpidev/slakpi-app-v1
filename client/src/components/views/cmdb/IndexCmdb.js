import React, { Fragment } from 'react';

const IndexCmdb = () => {
    return (
        <Fragment>
            {/* Page Heading */}
            <div className="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 className="h3 mb-0 text-white">Menú principal de la CMDB</h1>
            </div>

            {/* CARD */}
            <div className="col-md-12">
                <div className="bg-black2 border-0 card shadow mb-4">
                    <div className="card-body3 border-black5">
                        <div className="row col-md-12">
                            <div className="col-xl-4 col-md-6 mb-4">
                                <a href="!#">
                                    <div className="card border-left-danger shadow h-100 py-2 bg-black border-top-0 border-bottom-0 border-right-0">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                        <div className="col mr-2">
                                            <div className="h5 mb-0 font-weight-bold text-white2 text-center">Contactos y usuarios</div>
                                        </div>
                                        <div className="col-auto center">
                                            <img src="assets/images/icons/contact.png" width="46px" alt="dummy" data-sr="bottom" />
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </a>
                            </div>
                            <div className="col-xl-4 col-md-6 mb-4">
                                <a href="!#">
                                    <div className="card border-left-danger shadow h-100 py-2 bg-black border-top-0 border-bottom-0 border-right-0">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                        <div className="col mr-2">
                                            <div className="h5 mb-0 font-weight-bold text-white2 text-center">Componentes infraestructura</div>
                                        </div>
                                        <div className="col-auto center">
                                            <img src="assets/images/icons/laptop.png" width="44px" alt="dummy" data-sr="bottom" />
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </a>
                            </div>
                            <div className="col-xl-4 col-md-6 mb-4">
                                <a href="!#">
                                    <div className="card border-left-danger shadow h-100 py-2 bg-black border-top-0 border-bottom-0 border-right-0">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                        <div className="col mr-2">
                                            <div className="h5 mb-0 font-weight-bold text-white2 text-center">Gestión de SLAs</div>
                                        </div>
                                        <div className="col-auto center">
                                            <img src="assets/images/icons/clock.png" width="46px" alt="dummy" data-sr="bottom" />
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div className="row col-md-12">
                            <div className="col-xl-4 col-md-6 mb-4">
                                <a href="!#">
                                    <div className="card border-left-danger shadow h-100 py-2 bg-black border-top-0 border-bottom-0 border-right-0">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                        <div className="col mr-2">
                                            <div className="h5 mb-0 font-weight-bold text-white2 text-center">Notificaciones de usuarios</div>
                                        </div>
                                        <div className="col-auto center">
                                            <img src="assets/images/icons/notification.png" width="46px" alt="dummy" data-sr="bottom" />
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </a>
                            </div>
                            <div className="col-xl-4 col-md-6 mb-4">
                                <a href="!#">
                                    <div className="card border-left-danger shadow h-100 py-2 bg-black border-top-0 border-bottom-0 border-right-0">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                        <div className="col mr-2">
                                            <div className="h5 mb-0 font-weight-bold text-white2 text-center">Roles de usuario</div>
                                        </div>
                                        <div className="col-auto center">
                                            <img src="assets/images/icons/access.png" width="46px" alt="dummy" data-sr="bottom" />
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </a>
                            </div>
                            <div className="col-xl-4 col-md-6 mb-4">
                                <a href="/companies">
                                    <div className="card border-left-danger shadow h-100 py-2 bg-black border-top-0 border-bottom-0 border-right-0">
                                    <div className="card-body">
                                        <div className="row no-gutters align-items-center">
                                        <div className="col mr-2">
                                            <div className="h5 mb-0 font-weight-bold text-white2 text-center">Organizaciones</div>
                                        </div>
                                        <div className="col-auto center">
                                            <img src="assets/images/icons/building.png" width="45px" alt="dummy" data-sr="bottom" />
                                        </div>
                                        </div>
                                    </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </Fragment>
    );
};

export default IndexCmdb;