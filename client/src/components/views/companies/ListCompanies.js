import React, { Fragment, useEffect } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { GET_COMPANIES } from '../../../queries/Companies';

const ListCompanies = () => {

    const { data, loading, error } = useQuery(GET_COMPANIES);
    console.log(data)

    return (
        <Fragment>
            {/* Page Heading */}
            <div className="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 className="h3 mb-0 text-white">Listado: Organizaciones</h1>
                <a href="!#" className="btn btn-black2 btn-icon-split">
                    <span className="icon text-white-50">
                    <i className="fas fa-download" /></span>
                    <span className="text">Generar reportes</span>
                </a>
            </div>
            <div className="col-md-12">
                <div className="bg-black2 border-0 card shadow mb-4">
                    <div className="card-body">
                        {/* Tablas */}
                        <div className="block col-md-12">
                            <a className="btn btn-sm btn-black" href="/companies/create" role="button" id="btnCrearTicket">
                                <span className="text"><i className="fas fa-plus-square"></i> Crear Organización</span>
                            </a>
                            <hr />
                            <div className="table-responsive">
                                <table className="table-hover table table-sm rounded table-black">
                                    <thead className="bg-gradient-warning text-black">
                                        <tr>
                                        <th scope="col">Nombre de la Organización</th>
                                        <th scope="col">Tipo de Organización</th>
                                        <th scope="col">Estado</th>
                                        <th scope="col">Ubicación principal</th>
                                        <th scope="col"><i className="fas fa-pen" /></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {data.getAllCompanies.map(company => (
                                                <tr key = { company.id } >
                                                    <th className="text-uppercase" scope="row"> { company.name } </th>
                                                    <td>{ company.type_comp } </td>
                                                    <td>{ (company.status === "true" ? 'Activo' : 'Inactivo') } </td>
                                                    <td>{ (company.ubication !== null) ? company.ubication : 'Sin Ubicación' } </td>
                                                    <td><i className="fas fa-edit" /></td>
                                                </tr>                                        
                                            ))}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
};

export default ListCompanies;