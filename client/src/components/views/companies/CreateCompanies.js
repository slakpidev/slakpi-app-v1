import React, { Fragment, useState } from 'react';
import { NEW_COMPANIES } from '../../../mutations/Companies';
import { useMutation } from '@apollo/react-hooks';



const CreateCompanies = () => {


    const [company, addCompany] = useState({
        name: '',
        status: true,
        type_comp: ''
    });

    const updateStateCompanies = e => {
        addCompany({
            ...company,
            [e.target.name] : e.target.value
        })
    };

    const [createCompany] = useMutation(NEW_COMPANIES);

    const saveCompany = e =>{
        e.preventDefault();
        createCompany({ variables: { input: company } });
    };

    return (
        <Fragment>
          {/* Page Heading */}
          <div className="col-md-12">
            <div className="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 className="h3 mb-0 text-white">Crear Organización</h1>
            </div>
            <div className="bg-black2 border-0 card shadow mb-4 w-100">
                <div className="card-body">
                    <form onSubmit={saveCompany}>
                        <div className="row">
                            <div className="col-md-4">
                                <div className="form-group">
                                    <label className="text-gray-600" htmlFor="txtAsunto">Nombre de la Organización</label>
                                    <input 
                                        type="text" 
                                        className="bg-black text-gray-400 form-control" 
                                        placeholder="Ingrese el Nombre de la Organización" 
                                        id="name"
                                        name="name"
                                        onChange={updateStateCompanies}
                                    />
                                </div>
                            </div>
                            <div className="col-md-4">
                                <div className="form-group select-black">
                                    <label className="text-gray-600" htmlFor="ddlEstado">Estado</label>
                                    <select 
                                        className="bg-black text-gray-600 form-control" 
                                        id="ddlEstado" 
                                        name="status"
                                        onChange={updateStateCompanies}
                                        >
                                        <option value> Seleccione </option>
                                        <option value={true}> Activo</option>
                                        <option value={false}> Inactivo</option>
                                    </select>
                                </div>
                            </div>
                            <div className="col-md-4">                            
                                <div className="form-group select-black">
                                    <label className="text-gray-600" htmlFor="ddlTipoOrganizacion">Tipo de Organización</label>
                                    <select 
                                        className="bg-black text-gray-600 form-control" 
                                        id="ddlTipoOrganizacion" 
                                        name="type_comp"
                                        onChange={updateStateCompanies}
                                        >
                                        <option>Seleccione</option>
                                        <option value="Organización Principal">Organización Principal</option>
                                        <option value="Proveedor de soporte externo">Proveedor de soporte externo</option>
                                    </select>
                                </div>
                            </div>

                            <div className="col-md-12">
                                <button 
                                    type="submit" 
                                    className="btn btn-outline-success"                                        
                                >
                                    Crear Organización
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        </Fragment>
    );
};

export default CreateCompanies;