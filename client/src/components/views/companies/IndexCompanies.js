import React, {Fragment} from 'react';

const IndexCompanies = () => {
    return (
        <Fragment>
            {/* Page Heading */}
            <div className="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 className="h3 mb-0 text-white">Organizaciones, áreas y lugares</h1>
            </div>
            {/* CARD */}
            <div className="col-md-12">
                <div className="bg-black2 border-0 card shadow mb-4">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-md-4 mb-4">
                                <a href="/companies/list">
                                    <div className="card border-left-danger shadow h-100 py-2 bg-black border-top-0 border-bottom-0 border-right-0">
                                        <div className="card-body">
                                            <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="h5 mb-0 font-weight-bold text-white2 text-center">Organizaciones</div>
                                            </div>
                                            <div className="col-auto center">
                                                <img src="assets/images/icons/building.png" width="45px" alt="dummy" data-sr="bottom" />
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div className="col-md-4 mb-4">
                                <a href="!#">
                                    <div className="card border-left-danger shadow h-100 py-2 bg-black border-top-0 border-bottom-0 border-right-0">
                                        <div className="card-body">
                                            <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="h5 mb-0 font-weight-bold text-white2 text-center">Departamentos</div>
                                            </div>
                                            <div className="col-auto center">
                                                <img src="assets/images/icons/area.png" width="46px" alt="dummy" data-sr="bottom" />
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div className="col-md-4 mb-4">
                                <a href="!#">
                                    <div className="card border-left-danger shadow h-100 py-2 bg-black border-top-0 border-bottom-0 border-right-0">
                                        <div className="card-body">
                                            <div className="row no-gutters align-items-center">
                                            <div className="col mr-2">
                                                <div className="h5 mb-0 font-weight-bold text-white2 text-center">Ubicaciones</div>
                                            </div>
                                            <div className="col-auto center">
                                                <img src="assets/images/icons/pin.png" width="44px" alt="dummy" data-sr="bottom" />
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </Fragment>
    );
};

export default IndexCompanies;