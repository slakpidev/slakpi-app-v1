import React, {Fragment, useState} from 'react';
import Formsearch from './Formsearch';
import Alertnotifications from '../notifications/Alertnotifications';
import Messagesnotifications from '../notifications/Messagesnotifications';
import Chatnotifications from '../notifications/Chatnotifications';
import Userinformation from './Userinformation';

const Header =  ({sideBarMenu, session}) => {

    // Create the Hook for the sidebar toggle
    const [toggle, addToggleClass] = useState({
        class : 'toggled'
    });

    const addClassToggled = e => {
        e.preventDefault();
        if(toggle.class === ''){
            addToggleClass({
                class : 'toggled'
            })
        }else{
            addToggleClass({
                class : ''
            })
        }
        // Send the value toggle to the App.js for add into the Sidebar.js
        sideBarMenu(toggle.class)
    }
    
    // Verifications for the Permissions and header Bar
    
    const NavNotAuth = () => (
        <div></div>
    );
    const NavAuth = () => (
        <div>
            <nav className="navbar navbar-expand navbar-light bg-base-nav-gradient topbar mb-4 static-top shadow">
                {/* ----------  Sidebar Toggle (Topbar)  ---------- */}
                <button 
                    id="sidebarToggleTop" 
                    className="btn btn-link rounded-circle mr-3 text-success"
                    onClick={addClassToggled}
                >
                    <i className="fa fa-bars" />
                </button>
                {/* ----------  Topbar Search  ---------- */}
                <Formsearch />
                {/* ----------  Topbar Navbar  ---------- */}
                <ul className="navbar-nav ml-auto">
                    {/* ----------  Nav Item - Search Dropdown (Visible Only XS)  ---------- */}
                    <li className="nav-item dropdown no-arrow d-sm-none">
                        <a className="nav-link dropdown-toggle" href="#!" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fas fa-search fa-fw" />
                        </a>
                        {/* ----------  Dropdown - Messages  ---------- */}
                        <div className="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
                            <form className="form-inline mr-auto w-100 navbar-search">
                                <div className="input-group">
                                    <input type="text" className="form-control bg-light border-0 small" placeholder="Search for..." aria-label="Search" aria-describedby="basic-addon2" />
                                    <div className="input-group-append">
                                    <button className="btn btn-primary" type="button">
                                        <i className="fas fa-search fa-sm" />
                                    </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </li>
                    {/* ----------  Nav Item - Alerts  ---------- */}
                    <li className="nav-item dropdown no-arrow mx-1">
                        <a className="nav-link dropdown-toggle" href="#!" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fas fa-bell fa-fw" />
                            {/* ----------  Counter - Alerts  ---------- */}
                            <span className="badge badge-danger badge-counter">3+</span>
                        </a>
                        {/* ----------  Dropdown - Alerts  ---------- */}
                        <Alertnotifications />
                    </li>
                    {/* ----------  Nav Item - Messages  ---------- */}
                    <li className="nav-item dropdown no-arrow mx-1">
                        <a className="nav-link dropdown-toggle" href="#!" id="messagesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fas fa-envelope fa-fw" />
                            {/* ----------  Counter - Messages  ---------- */}
                            <span className="badge badge-danger badge-counter">7</span>
                        </a>
                        {/* ----------  Dropdown - Messages  ---------- */}
                        <Messagesnotifications />
                    </li>
                    {/* ----------  Nav Item - Chat  ---------- */}
                    <li className="nav-item dropdown no-arrow mx-1">
                        <a className="nav-link dropdown-toggle" href="#!" id="chatDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <i className="fas fa-comments fa-fw" />
                            {/* ----------  Counter - Chat  ---------- */}
                            <span className="badge badge-danger badge-counter">23</span>
                        </a>
                        {/* ----------  Dropdown - Chat  ---------- */}
                        <Chatnotifications />
                    </li>
                    <div className="topbar-divider d-none d-sm-block" />
                    {/* ----------  Nav Item - User Information  ---------- */}
                    <li className="nav-item dropdown no-arrow">
                        <a className="nav-link dropdown-toggle" href="#!" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span className="mr-2 d-none d-lg-inline text-gray-400 small">{session.getUserLogin.first_name + ' ' + session.getUserLogin.last_name }</span>
                            <img className="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60" alt="fake" />
                        </a>
                        {/* ----------  Dropdown - User Information  ---------- */}
                        <Userinformation />
                    </li>
                </ul>
            </nav>
        </div>
    );

    let headerBar = (session.getUserLogin) ? <NavAuth /> : <NavNotAuth />

    return (
        <Fragment>            
            {headerBar}
        </Fragment>
    );
};

export default Header;