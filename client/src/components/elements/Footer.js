import React, { Fragment } from 'react';

const Footer = ({session}) => {

    const FooterNoAuth = () => (
        <div></div>
    );
    const FooterAuth = () => (
    <footer className="sticky-footer bg-base-nav-gradient">
        <div className="container my-auto">
            <div className="copyright text-center my-auto text-gray-600">
            <span>Copyright © Slakpi Tech</span>
            </div>
        </div>
    </footer>
    )
    let footer = (session.getUserLogin) ? <FooterAuth /> : <FooterNoAuth />
    return (
        <Fragment>
            {/* Footer */}
                {footer} 
            {/* End of Footer */}
        </Fragment>
    );
};

export default Footer;