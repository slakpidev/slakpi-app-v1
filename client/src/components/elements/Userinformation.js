import React, { Fragment, useState } from 'react';
import { Modal, Button } from 'react-bootstrap';
import Logout from '../views/users/Logout';

const Userinformation = () => {

    const [show, setShow] = useState(false);

    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);

    return (
        <Fragment>            
            <div className="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a className="dropdown-item" href="#!">
                <i className="fas fa-user fa-sm fa-fw mr-2 text-gray-400" />
                Profile
                </a>
                <a className="dropdown-item" href="#!">
                <i className="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400" />
                Settings
                </a>
                <a className="dropdown-item" href="#!">
                <i className="fas fa-list fa-sm fa-fw mr-2 text-gray-400" />
                Activity Log
                </a>
                <div className="dropdown-divider" />
                <Button variant="link" className="dropdown-item" onClick={handleShow}>
                    <i className="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400" />
                    Logout
                </Button>
            </div>
            {/* Logout Modal*/}
            <Modal show={show} onHide={handleClose} className="rounded-0 border-0">
                <Modal.Header closeButton className="bg-gray-600 border-0 rounded-0 text-gray-200">
                    <Modal.Title>Cerrar Sesión</Modal.Title>
                </Modal.Header>
                <Modal.Body className="bg-black">¿Seguro que desea cerrar sesión?</Modal.Body>
                <Modal.Footer className="rounded-0 border-0 bg-black2">
                    <Button variant="secondary" onClick={handleClose}>
                        Cancelar
                    </Button>
                    <Logout />
                </Modal.Footer>
            </Modal>

        </Fragment>
    );
};

export default Userinformation;