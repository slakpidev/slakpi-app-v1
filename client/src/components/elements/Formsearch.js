import React, {Fragment} from 'react';

const Formsearch = () => {
    return (
        <Fragment>            
            <form className="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100">
                <div className="input-group">
                <input type="text" className="form-control bg-black border-0 small" placeholder="Buscar..." aria-label="Search" aria-describedby="basic-addon2" />
                <div className="input-group-append">
                    <button className="btn btn-black3" type="button">
                    <i className="fas fa-search fa-sm" />
                    </button>
                </div>
                </div>
            </form>
        </Fragment>
    );
};

export default Formsearch;