import Dashboard from '../views/Dashboard';
import IndexTicket from '../views/tickets/IndexTicket';
import CreateTicket from '../views/tickets/CreateTicket';
import CreateCompanies from '../views/companies/CreateCompanies';
import IndexCompanies from '../views/companies/IndexCompanies';
import ListCompanies from '../views/companies/ListCompanies';
import IndexCmdb from '../views/cmdb/IndexCmdb';
import IndexUser from '../views/users/IndexUser';

const ArrayRoute = [      
        { path: '/dashboard', name: 'Dashboard', slug: 'dashboard', subLink: false ,Component: Dashboard, icons: 'fa-tachometer-alt'},
        { path: '/users', name: 'Usuarios Administradores', slug: 'users', subLink: false ,Component: IndexUser, icons: 'fa-users'},
        { path: '/users/create', name: 'Crear Usuarios Administradores', slug: 'users-create', subLink: true ,Component: Dashboard, icons: 'fa-user-plus'},
        { path: '/tickets', name: 'Tickets', slug: 'tickets', subLink: false ,Component: IndexTicket, icons: 'fa-ticket-alt'},
        { path: '/tickets/create', name: '', slug: 'tickets-create', subLink: true, Component: CreateTicket, icons: ''},
        { path: '/cmdb', name: 'CMDB', slug: 'cmdb', subLink: false, Component: IndexCmdb, icons: 'fa-database'},
        { path: '/companies', name: 'Organización', slug: 'companies', subLink: true, Component: IndexCompanies, icons: ''},
        { path: '/companies/list', name: 'Organizaciones', slug: 'companies-list', subLink: true, Component: ListCompanies, icons: ''},
        { path: '/companies/create', name: 'Agregar Organización', slug: 'companies-create', subLink: true, Component: CreateCompanies, icons: ''},       
];

export default ArrayRoute;