import React, {useState} from 'react';
import { Toast } from 'react-bootstrap';

const ErrorMessages = ({ type, title, message }) => {

    const [showToast, setShowToast] = useState(true);
    const toggleShowA = () => setShowToast(!showToast);

    return (
        <Toast className="bg-black2" show={showToast} delay={5000} autohide onClose={toggleShowA} style={{
            position: 'absolute',
            top: 0,
            right: 0,
            }}>
            <Toast.Header className= {`bg-secondary text-gray-400 border-0 border-left-${type}`}>
                <i className={`fas fa-exclamation-triangle mr-2 mt-1 text-${type}`}></i>
                <strong className="mr-auto">{ title }</strong>
            </Toast.Header>
            <Toast.Body>{ message }</Toast.Body>
        </Toast>
    );
};

export default ErrorMessages;