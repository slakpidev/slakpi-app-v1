import React from 'react';
import { Route, Switch } from "react-router-dom";

import Login from '../views/users/Login';


const Routes = ({refetch, session, ArrayRoute}) => {
    return (
        <Switch>
            {ArrayRoute.map(({ path, Component }) => (
                <Route key={path} exact path={path} component={Component} />            
            ))}
            <Route exact path="/login" render= { () => <Login refetch={refetch} session = {session} /> } />
        </Switch>
    );
};

export default Routes;