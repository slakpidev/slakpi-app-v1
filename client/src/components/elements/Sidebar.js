import React, {Fragment} from 'react';

const Sidebar = ({toggleClass, session, ArrayRoute}) => {

    const SidebarNotAuth = () => (
        <div></div>
    );
    const MenuList = () => ArrayRoute.map(route => {
        if(!route.subLink){
            return (
                    <li key = {route.path} className="nav-item" data-toggle="collapse" data-target = {`#${route.slug}`} aria-expanded="true" aria-controls={route.slug}>
                        <a className="nav-link" href={route.path}>
                            <i className ={`fas fa-fw ${route.icons}`} />
                            <span> {route.name} </span>
                        </a>
                    </li>
                )
            }
        })
    const SidebarAuth = () => (
        <div>
            <ul className={toggleClass.toggleClass + ' navbar-nav bg-base-nav sidebar sidebar-dark accordion'} id="accordionSidebar">
                {/* Sidebar - Brand */}
                <a className="sidebar-brand d-flex align-items-center justify-content-center" href="/">
                    <div className="sidebar-brand-icon">
                        <img src="/assets/images/site-header-icon.png" style={{width: 40}} alt="Slakpi logo" />
                    </div>
                    <div className="sidebar-brand-text mx-3"><img src="/assets/images/site-header-logo.png" style={{width: 100}} alt="Slakpi logo" /></div>
                </a>
                {/* Divider */}
                <hr className="sidebar-divider my-0" />
                {/* Nav Item - Dashboard */}
                <MenuList />
            </ul>
        </div>
    );
    
    let sideBar = (session.getUserLogin) ? <SidebarAuth /> : <SidebarNotAuth />

    return (
        <Fragment>
             {sideBar}   
        </Fragment>
    );
};

export default Sidebar;